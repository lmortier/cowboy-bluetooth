package com.mortier.cowboy.bluetooth.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import com.mortier.cowboy.bluetooth.bluetooth.DeviceDetailsExtractor
import com.mortier.cowboy.bluetooth.data.DeviceDetails
import com.mortier.cowboy.bluetooth.data.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class DeviceDetailsViewModel @Inject constructor(private val deviceDetailsExtractor: DeviceDetailsExtractor) :
    ViewModel() {

    lateinit var macAddress: String

    private val _characteristics: LiveData<Result<DeviceDetails>> = liveData {
        emit(Result.Loading())
        emitSource(deviceDetailsExtractor.getDeviceDetails(macAddress)
            .map { Result.Success(it) }
            .catch { exception ->
                emit(Result.Error<DeviceDetails>(exception))
            }
            .flowOn(Dispatchers.IO)
            .asLiveData()
        )
    }

    val characteristics: LiveData<Result<DeviceDetails>> = _characteristics
}