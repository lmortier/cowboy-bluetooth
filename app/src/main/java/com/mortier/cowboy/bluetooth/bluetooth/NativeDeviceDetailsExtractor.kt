package com.mortier.cowboy.bluetooth.bluetooth

import android.bluetooth.*
import android.content.Context
import android.util.Log
import com.mortier.cowboy.bluetooth.data.DeviceConnectionState
import com.mortier.cowboy.bluetooth.data.DeviceDetails
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

@ExperimentalCoroutinesApi
class NativeDeviceDetailsExtractor(private val context: Context) : DeviceDetailsExtractor {

    companion object {
        private const val TAG = "NativeDeviceDetails"
    }

    private val bluetoothAdapter: BluetoothAdapter by lazy {
        BluetoothAdapter.getDefaultAdapter()
    }

    override fun getDeviceDetails(macAddress: String): Flow<DeviceDetails> = callbackFlow {
        val deviceDetails = DeviceDetails(macAddress, DeviceConnectionState.Connecting)
        var gatt: BluetoothGatt? = null
        val bluetoothGattCallback = object : BluetoothGattCallback() {
            override fun onConnectionStateChange(
                blGatt: BluetoothGatt?,
                status: Int,
                newState: Int
            ) {
                gatt = blGatt;
                Log.d(TAG, "Connection state changed")
                if (newState == BluetoothGatt.GATT_SUCCESS) {
                    if (newState == BluetoothProfile.STATE_CONNECTED) {
                        Log.d(TAG, "Connected!!!")
                        // successfully connected to the GATT Server
                        // Attempts to discover services after successful connection.
                        blGatt?.discoverServices()
                    } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                        Log.d(TAG, "Not Connected, status code $status")
                        deviceDetails.connectionState = DeviceConnectionState.Disconnected
                        offer(deviceDetails)
                        val exception =
                            IllegalStateException("Cannot connect to device. Status code: $status")
                        close(exception)
                    }
                }
            }

            override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
                deviceDetails.connectionState = DeviceConnectionState.Connected
                offer(deviceDetails)

                val services = gatt?.services ?: return
                Log.w(TAG, "Discovered ${services.size} services")
                deviceDetails.characteristics = gatt.getGattServicesAndCharacteristics()
                offer(deviceDetails)
            }
        }

        offer(deviceDetails)

        val device = connectToDevice(macAddress)
        deviceDetails.name = device.name
        offer(deviceDetails)

        device.connectGatt(context, false, bluetoothGattCallback) ?: run {
            close(IllegalStateException("Unable to connect to the device"))
            return@callbackFlow
        }

        awaitClose {
            Log.d(TAG, "Cleaning up details")
            gatt?.close()
            gatt = null
        }
    }

    private fun connectToDevice(macAddress: String): BluetoothDevice {
        return bluetoothAdapter.getRemoteDevice(macAddress)
    }

    private fun BluetoothGatt.getGattServicesAndCharacteristics(): List<String> {
        if (services.isEmpty()) {
            Log.i(
                "printGattTable",
                "No service and characteristic available, call discoverServices() first?"
            )
            return emptyList()
        }

        return services.map { service ->
            val characteristicsTable = service.characteristics.joinToString(
                separator = "\n|--",
                prefix = "|--"
            ) { it.uuid.toString() }

            "Service ${service.uuid}\nCharacteristics:\n$characteristicsTable"
        }
    }
}