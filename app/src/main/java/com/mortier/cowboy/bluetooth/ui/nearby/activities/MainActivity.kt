package com.mortier.cowboy.bluetooth.ui.nearby.activities

import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.mortier.cowboy.bluetooth.R
import com.mortier.cowboy.bluetooth.ui.nearby.fragments.NearbyDevicesFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                add(R.id.fragment_container, NearbyDevicesFragment())
            }
        }
    }
}