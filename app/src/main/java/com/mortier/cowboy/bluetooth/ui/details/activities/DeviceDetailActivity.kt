package com.mortier.cowboy.bluetooth.ui.details.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.mortier.cowboy.bluetooth.R
import com.mortier.cowboy.bluetooth.ui.details.fragments.DeviceDetailFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DeviceDetailActivity : AppCompatActivity(R.layout.activity_main) {

    companion object {
        private const val KEY_DEVICE_MAC_ADDRESS = "DEVICE_MAC_ADDRESS"

        fun newInstance(context: Context?, macAddress: String): Intent {
            val intent = Intent(context, DeviceDetailActivity::class.java)
            intent.putExtra(KEY_DEVICE_MAC_ADDRESS, macAddress)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            val macAddress = intent?.getStringExtra(KEY_DEVICE_MAC_ADDRESS) ?: run {
                finish()
                return
            }

            supportFragmentManager.commit {
                add(R.id.fragment_container, DeviceDetailFragment.newInstance(macAddress))
            }
        }
    }
}