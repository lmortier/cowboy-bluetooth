package com.mortier.cowboy.bluetooth.ui.nearby.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mortier.cowboy.bluetooth.R
import com.mortier.cowboy.bluetooth.data.NearbyDevice
import com.mortier.cowboy.bluetooth.ui.nearby.viewholders.NearbyDeviceViewHolder

class DevicesAdapter(
    itemCallback: DiffUtil.ItemCallback<NearbyDevice> = NearbyDeviceDiffUtil()
) : ListAdapter<NearbyDevice, NearbyDeviceViewHolder>(itemCallback) {

    var onNearbyDeviceClicked: (NearbyDevice) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NearbyDeviceViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.listitem_nearby_device, parent, false)
        return NearbyDeviceViewHolder(view).apply {
            this.itemView.setOnClickListener {
                val position = this.adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    onNearbyDeviceClicked(getItem(position))
                }
            }
        }
    }

    override fun onBindViewHolder(holder: NearbyDeviceViewHolder, position: Int) {
        holder.bindData(getItem(position))
    }

    private class NearbyDeviceDiffUtil : DiffUtil.ItemCallback<NearbyDevice>() {
        override fun areItemsTheSame(oldItem: NearbyDevice, newItem: NearbyDevice): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: NearbyDevice, newItem: NearbyDevice): Boolean {
            return oldItem.name == newItem.name
        }
    }
}