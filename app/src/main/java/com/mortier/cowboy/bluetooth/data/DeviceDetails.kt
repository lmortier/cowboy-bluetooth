package com.mortier.cowboy.bluetooth.data

data class DeviceDetails(
    val macAddress: String,
    var connectionState: DeviceConnectionState,
    var name: String = "",
    var characteristics: List<String> = emptyList()
)

enum class DeviceConnectionState {
    Connecting,
    Connected,
    Disconnected
}
