package com.mortier.cowboy.bluetooth.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import com.mortier.cowboy.bluetooth.bluetooth.DeviceDiscoverer
import com.mortier.cowboy.bluetooth.data.NearbyDevice
import com.mortier.cowboy.bluetooth.data.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import kotlin.math.abs

@HiltViewModel
class NearbyDevicesViewModel @Inject constructor(
    private val deviceDiscoverer: DeviceDiscoverer
): ViewModel() {

    private val _devicesLiveData: LiveData<Result<List<NearbyDevice>>> = liveData {
        emit(Result.Loading())
        emitSource(
            deviceDiscoverer.findDevices()
                .map { Result.Success(it.sortedBy { abs(it.rssi ?: 0) }) }
                .catch { exception ->
                    Result.Error<List<NearbyDevice>>(exception)
                }
                .flowOn(Dispatchers.IO)
                .asLiveData()
        )
    }
    val devicesLiveData: LiveData<Result<List<NearbyDevice>>> = _devicesLiveData

    fun refreshList() {
        deviceDiscoverer.clearScannedDevices()
    }
}