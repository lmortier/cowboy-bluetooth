package com.mortier.cowboy.bluetooth.ui.nearby.viewholders

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.mortier.cowboy.bluetooth.R
import com.mortier.cowboy.bluetooth.data.NearbyDevice

class NearbyDeviceViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    private val nameTextView: AppCompatTextView by lazy {
        itemView.findViewById(R.id.tv_device_name)
    }
    private val rssiTextView: AppCompatTextView by lazy {
        itemView.findViewById(R.id.tv_rssi)
    }
    private val canConnectTextView: AppCompatTextView by lazy {
        itemView.findViewById(R.id.tv_can_connect)
    }

    fun bindData(device: NearbyDevice) {
        nameTextView.text = device.name
        rssiTextView.text = itemView.context.getString(R.string.device_rssi)
            .replace("{rssi}", device.rssi.toString())

        if (device.canConnect) {
            canConnectTextView.isVisible = true
            canConnectTextView.setText(R.string.can_connect)
        } else {
            canConnectTextView.isVisible = false
        }
    }
}