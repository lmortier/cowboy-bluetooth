package com.mortier.cowboy.bluetooth.data

data class NearbyDevice(
    val id: String,
    val name: String,
    val rssi: Int? = null,
    val canConnect: Boolean = false
)
