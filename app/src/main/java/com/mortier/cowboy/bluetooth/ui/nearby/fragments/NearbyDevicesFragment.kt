package com.mortier.cowboy.bluetooth.ui.nearby.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.mortier.cowboy.bluetooth.data.Result
import com.mortier.cowboy.bluetooth.databinding.FragmentNearbyDevicesBinding
import com.mortier.cowboy.bluetooth.ui.details.activities.DeviceDetailActivity
import com.mortier.cowboy.bluetooth.ui.nearby.adapters.DevicesAdapter
import com.mortier.cowboy.bluetooth.viewmodels.NearbyDevicesViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlin.properties.Delegates

@AndroidEntryPoint
class NearbyDevicesFragment : Fragment() {

    private lateinit var binding: FragmentNearbyDevicesBinding

    private val nearbyDevicesViewModel: NearbyDevicesViewModel by viewModels()

    private val rvNearbyDevices: RecyclerView by lazy { binding.rvNearbyDevices }
    private val refreshLayout: SwipeRefreshLayout by lazy { binding.refreshLayout }

    private val devicesAdapter: DevicesAdapter by lazy {
        DevicesAdapter().apply {
            this.onNearbyDeviceClicked = { device ->
                startActivity(DeviceDetailActivity.newInstance(context, device.id))
            }
        }
    }

    private var loading: Boolean by Delegates.observable(true) { _, old, new ->
        if (old != new) {
            refreshLayout.isRefreshing = new
            rvNearbyDevices.isVisible = !new
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNearbyDevicesBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvNearbyDevices.apply {
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = devicesAdapter
        }

        nearbyDevicesViewModel.devicesLiveData.observe(viewLifecycleOwner, {
            when (it) {
                is Result.Error -> TODO()
                is Result.Loading -> {
                    loading = true
                }
                is Result.Success -> {
                    loading = false
                    devicesAdapter.submitList(it.data)
                }
            }
        })

        refreshLayout.setOnRefreshListener {
            nearbyDevicesViewModel.refreshList()
            refreshLayout.isRefreshing = false
        }
    }
}