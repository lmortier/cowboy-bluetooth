package com.mortier.cowboy.bluetooth.data

sealed class Result<T> {
    class Loading<T>: Result<T>()
    class Success<T>(val data: T) : Result<T>()
    class Error<T>(val exception: Throwable) : Result<T>()
}