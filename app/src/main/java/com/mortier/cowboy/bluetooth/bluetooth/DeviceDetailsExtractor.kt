package com.mortier.cowboy.bluetooth.bluetooth

import com.mortier.cowboy.bluetooth.data.DeviceDetails
import kotlinx.coroutines.flow.Flow

interface DeviceDetailsExtractor {

    fun getDeviceDetails(macAddress: String): Flow<DeviceDetails>

}