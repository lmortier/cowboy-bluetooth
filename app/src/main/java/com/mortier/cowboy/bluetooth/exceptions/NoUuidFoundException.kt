package com.mortier.cowboy.bluetooth.exceptions

class NoUuidFoundException(macAddress: String) :
    IllegalArgumentException("No UUIDs found for device with mac address: $macAddress")