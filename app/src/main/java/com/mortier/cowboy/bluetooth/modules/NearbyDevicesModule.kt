package com.mortier.cowboy.bluetooth.modules

import android.content.Context
import com.mortier.cowboy.bluetooth.bluetooth.DeviceDetailsExtractor
import com.mortier.cowboy.bluetooth.bluetooth.DeviceDiscoverer
import com.mortier.cowboy.bluetooth.bluetooth.NativeDeviceDiscoverer
import com.mortier.cowboy.bluetooth.bluetooth.RxDeviceDetailsExtractor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NearbyDevicesModule {

    @ExperimentalCoroutinesApi
    @Singleton
    @Provides
    fun bindDeviceDiscoverer(): DeviceDiscoverer {
        return NativeDeviceDiscoverer()
    }

    @ExperimentalCoroutinesApi
    @Provides
    @Singleton
    fun bindDeviceDetailExtractor(@ApplicationContext context: Context): DeviceDetailsExtractor {
        return RxDeviceDetailsExtractor(context)
    }

}