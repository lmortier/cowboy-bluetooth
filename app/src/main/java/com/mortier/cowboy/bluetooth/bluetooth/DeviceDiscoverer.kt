package com.mortier.cowboy.bluetooth.bluetooth

import com.mortier.cowboy.bluetooth.data.NearbyDevice
import kotlinx.coroutines.flow.Flow

interface DeviceDiscoverer {

    suspend fun findDevices(): Flow<List<NearbyDevice>>

    fun clearScannedDevices()
}