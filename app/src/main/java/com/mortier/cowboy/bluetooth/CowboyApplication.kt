package com.mortier.cowboy.bluetooth

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CowboyApplication: Application() {
}