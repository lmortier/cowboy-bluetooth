package com.mortier.cowboy.bluetooth.bluetooth

import android.app.Application
import android.util.Log
import com.mortier.cowboy.bluetooth.data.NearbyDevice
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.scan.ScanFilter
import com.polidea.rxandroidble2.scan.ScanSettings
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RxAndroidBleDeviceDiscoverer @Inject constructor(application: Application) : DeviceDiscoverer {

    companion object {
        private const val TAG = "RxDeviceDiscoverer"
    }

    private val client: RxBleClient by lazy {
        RxBleClient.create(application)
    }

    private val scanSettings: ScanSettings by lazy {
        ScanSettings.Builder()
            .build()
    }

    private var scanDisposable: Disposable? = null
    private val foundDevices: MutableMap<String, NearbyDevice> = mutableMapOf()

    override suspend fun findDevices(): Flow<List<NearbyDevice>> = callbackFlow {
        val filter = ScanFilter.Builder()
            .build()
        scanDisposable = client.scanBleDevices(scanSettings, filter)
            .map {
                val id = it.bleDevice.macAddress
                NearbyDevice(
                    id,
                    it.bleDevice.name ?: "<Unknown>",
                    it.rssi
                )
            }
            .doOnNext {
                foundDevices[it.id] = it
            }
            .buffer(1L, TimeUnit.SECONDS)
            .subscribe({ nearbyDevice ->
                offer(foundDevices.values.toList())
            }, {
                close(it)
            }, {
                close()
            })

        awaitClose {
            Log.d(TAG, "Stopped scanning")
            scanDisposable?.dispose()
        }
    }

    override fun clearScannedDevices() {
        foundDevices.clear()
    }
}