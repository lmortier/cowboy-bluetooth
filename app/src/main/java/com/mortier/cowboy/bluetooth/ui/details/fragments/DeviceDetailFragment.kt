package com.mortier.cowboy.bluetooth.ui.details.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.snackbar.Snackbar
import com.mortier.cowboy.bluetooth.R
import com.mortier.cowboy.bluetooth.data.DeviceConnectionState
import com.mortier.cowboy.bluetooth.data.DeviceDetails
import com.mortier.cowboy.bluetooth.data.Result
import com.mortier.cowboy.bluetooth.databinding.FragmentDeviceDetailsBinding
import com.mortier.cowboy.bluetooth.viewmodels.DeviceDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DeviceDetailFragment : Fragment() {

    companion object {
        private const val KEY_DEVICE_MAC_ADDRESS = "key_device_mac_address"

        fun newInstance(macAddress: String): DeviceDetailFragment {
            val args = Bundle()
            args.putString(KEY_DEVICE_MAC_ADDRESS, macAddress)
            val fragment = DeviceDetailFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var bindings: FragmentDeviceDetailsBinding
    private val viewModel: DeviceDetailsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentDeviceDetailsBinding.inflate(inflater).also {
        this.bindings = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val macAddress = arguments?.getString(KEY_DEVICE_MAC_ADDRESS)
        if (macAddress.isNullOrBlank()) {
            activity?.finish()
            return
        }

        viewModel.macAddress = macAddress
        viewModel.characteristics.observe(viewLifecycleOwner, {
            when (it) {
                is Result.Error -> {
                    Log.e("Details", it.exception.message, it.exception)
                    Snackbar.make(
                        bindings.root,
                        it.exception.message ?: "Something went wrong",
                        Snackbar.LENGTH_SHORT
                    ).show()

                    bindings.loadingDetails.isVisible = false
                }
                is Result.Loading -> {
                    bindings.loadingDetails.isVisible = true
                }
                is Result.Success -> {
                    Log.d("Details", "Got characteristics: ${it.data}")
                    updateUiForDevice(it.data)
                }
            }
        })
    }

    private fun updateUiForDevice(device: DeviceDetails) {
        bindings.tvDeviceName.text = device.name
        when (device.connectionState) {
            DeviceConnectionState.Connecting -> {
                bindings.tvConnectionStatus.setText(R.string.device_connecting)
                bindings.loadingDetails.isVisible = true
            }

            DeviceConnectionState.Connected -> {
                bindings.tvConnectionStatus.setText(R.string.device_connected)
                bindings.loadingDetails.isVisible = false

                bindings.tvCharacteristics.text = device.characteristics.joinToString("\n\n")
            }

            DeviceConnectionState.Disconnected -> {
                bindings.tvConnectionStatus.setText(R.string.device_disconnected)
                bindings.loadingDetails.isVisible = false
                val snackbar = Snackbar.make(
                    bindings.root,
                    R.string.device_disconnected_message,
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.setAction(android.R.string.cancel) {
                    activity?.finish()
                }

                snackbar.show()
            }
        }
    }
}