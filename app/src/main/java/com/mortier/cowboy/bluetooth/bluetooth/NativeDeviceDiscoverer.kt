package com.mortier.cowboy.bluetooth.bluetooth

import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.*
import android.util.Log
import com.mortier.cowboy.bluetooth.data.NearbyDevice
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow


@ExperimentalCoroutinesApi
class NativeDeviceDiscoverer : DeviceDiscoverer {

    companion object {
        private const val TAG = "NativeDiscoverer"
    }

    private val foundDevices: MutableMap<String, NearbyDevice> = mutableMapOf()

    override suspend fun findDevices(): Flow<List<NearbyDevice>> = callbackFlow {
        val currentThread = Thread.currentThread()
        Log.d(TAG, "Current thread: ${currentThread.name}")

        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter() ?: run {
            Log.d(TAG, "no bluetooth adapter found")
            close(IllegalStateException("No bluetooth adapter found"))
            return@callbackFlow
        }

        val scanCallback = object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult?) {
                super.onScanResult(callbackType, result)
                result?.let {
                    createNearbyDevices(mutableListOf(it))
                }
            }

            override fun onBatchScanResults(results: MutableList<ScanResult>?) {
                super.onBatchScanResults(results)
                results?.let {
                    createNearbyDevices(it)
                }
            }

            override fun onScanFailed(errorCode: Int) {
                super.onScanFailed(errorCode)
                close(IllegalStateException("Stopped scanning with error code $errorCode"))
            }

            private fun createNearbyDevices(results: MutableList<ScanResult>) {
                results
                    .map {
                        NearbyDevice(
                            it.device.address,
                            it.device.name ?: "<unknown>",
                            it.rssi,
                            it.isConnectable
                        )
                    }
                    .forEach {
                        foundDevices[it.id] = it
                    }

                offer(foundDevices.values.toList())
            }
        }
        bluetoothAdapter.bluetoothLeScanner.startScan(scanCallback)

        awaitClose {
            Log.d(TAG, "Stopped scanning")
            bluetoothAdapter.bluetoothLeScanner.stopScan(scanCallback)
        }
    }

    override fun clearScannedDevices() {
        foundDevices.clear()
    }
}