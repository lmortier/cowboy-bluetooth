package com.mortier.cowboy.bluetooth.bluetooth

import android.bluetooth.BluetoothGattService
import android.content.Context
import android.util.Log
import com.mortier.cowboy.bluetooth.data.DeviceConnectionState
import com.mortier.cowboy.bluetooth.data.DeviceDetails
import com.mortier.cowboy.bluetooth.exceptions.NoUuidFoundException
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.RxBleConnection
import com.polidea.rxandroidble2.Timeout
import com.polidea.rxandroidble2.utils.StandardUUIDsParser
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
class RxDeviceDetailsExtractor(context: Context) : DeviceDetailsExtractor {

    private companion object {
        private const val TAG = "RxDetails"
    }

    private val client by lazy {
        RxBleClient.create(context)
    }

    private val disposables: MutableList<Disposable> = mutableListOf()

    override fun getDeviceDetails(macAddress: String) = callbackFlow {
        try {
            val deviceDetail = DeviceDetails(
                macAddress = macAddress,
                connectionState = DeviceConnectionState.Connecting
            )
            offer(deviceDetail)
            val device = client.getBleDevice(macAddress)
            deviceDetail.name = device.name ?: "<unknown>"
            device.observeConnectionStateChanges()
                .subscribe({
                    when (it) {
                        RxBleConnection.RxBleConnectionState.CONNECTING -> {
                            deviceDetail.connectionState = DeviceConnectionState.Connecting
                        }
                        RxBleConnection.RxBleConnectionState.CONNECTED -> {
                            deviceDetail.connectionState = DeviceConnectionState.Connected
                        }
                        RxBleConnection.RxBleConnectionState.DISCONNECTED -> {
                            deviceDetail.connectionState = DeviceConnectionState.Disconnected
                        }
                        RxBleConnection.RxBleConnectionState.DISCONNECTING -> {
                            deviceDetail.connectionState = DeviceConnectionState.Disconnected
                        }
                        else -> {
                            // TODO add an extra state here
                        }
                    }

                    offer(deviceDetail)
                }, {
                    close(it)
                }, {
                    close()
                })
                .also {
                    disposables.add(it)
                }

            device.establishConnection(false, Timeout(30L, TimeUnit.SECONDS))
                .doOnNext {
                    val thread = Thread.currentThread()
                    Log.d(TAG, "Getting connection on thread: ${thread.name}")
                    deviceDetail.connectionState = DeviceConnectionState.Connected
                    offer(deviceDetail)
                }
                .flatMapSingle {
                    it.discoverServices(30L, TimeUnit.SECONDS)
                }
                .map {
                    val services = it.bluetoothGattServices
                    getGattServicesAndCharacteristics(services)
                }
                .subscribe({
                    deviceDetail.characteristics = it
                    offer(deviceDetail)
                }, {
                    close(it)
                }, {
                    close()
                })
                .apply {
                    disposables.add(this)
                }

            awaitClose {
                Log.d(TAG, "Cleaning up disposables")
                disposables.forEach { it.dispose() }
            }
        } catch (e: NoUuidFoundException) {
            close(e)
        }
    }

    private fun getGattServicesAndCharacteristics(services: List<BluetoothGattService>): List<String> {
        if (services.isEmpty()) {
            Log.i(TAG, "No service and characteristic available, call discoverServices() first?")
            return emptyList()
        }

        return services.mapNotNull { service ->
            val characteristicsTable = service.characteristics.joinToString(
                separator = "\n|--",
                prefix = "|--"
            ) {
                val characteristicName = StandardUUIDsParser.getCharacteristicName(it.uuid)
                val descriptorName = it.descriptors.mapNotNull {
                    StandardUUIDsParser.getDescriptorName(it.uuid)
                }.joinToString()

                "$characteristicName, $descriptorName"
            }

            val serviceName = StandardUUIDsParser.getServiceName(service.uuid)
            "Service ${serviceName}\nCharacteristics:\n$characteristicsTable"
        }
    }
}