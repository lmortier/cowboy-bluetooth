# Cowboy Bluetooth

## Goal
Make a sample app that demonstrates working with Bluetooth on Android

### View #1:
A list displaying all the Bluetooth devices reachable from your phone.
Bonus: You can also display the RSSI of every device you find in the main view and then sort them in real time based on that.

### View #2:
The second view is the detail of a device. Some of the devices you will find will be connectable (check the advertisement data).
If you can connect to the device we ask you to display all the services & characteristics from the device with all the relevant information (read, write, notification, name, etc).
When pressing the back button you should disconnect from the device and come back to the main view.


## Getting started
Put the app on your android phone (API 21+). The very first time the app will crash because it doesn't have the right permissions.
Go to:

* Phone settings
* Apps
* Find the app called "Cowboy Bluetooth"
* Go to permissions
* Allow location permissions
* Restart the app

## Inner workings

There are 2 ways to discover nearby devices. There is the native way, and an RxJava based way, using 
[RxAndroidBle](https://github.com/Polidea/RxAndroidBle). You can toggle between them in the `NearbyDevicesModule` class

| Way of discover        | Screen           | Status      |
| ---------------------- | ---------------- | ----------- |
| Native                 | Overview         | ✅ Working  |
| Native                 | Details          | 🔨 WIP      |
| Rx                     | Overview         | ✅ Working  |
| RX                     | Details          | ✅ Working  |
